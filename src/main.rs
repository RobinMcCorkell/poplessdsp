use std::{thread::sleep, time::Duration};

use rodio::{
    cpal::{default_host, traits::HostTrait},
    source::SineWave,
    DeviceTrait, OutputStream, Source,
};

const FREQUENCY_HZ: f32 = 10.0;
const AMPLIFICATION: f32 = 0.001;
const DEVICE_NAME: &str = "default:CARD=sndrpihifiberry";
const SLEEP_DURATION: Duration = Duration::from_secs(60 * 60 * 2);

fn main() {
    let device = default_host()
        .output_devices()
        .expect("output devices")
        .inspect(|device| println!("{:?}", device.name()))
        .find(|device| device.name().unwrap() == DEVICE_NAME)
        .expect("find device");
    let (_stream, stream_handle) =
        OutputStream::try_from_device(&device).expect("getting audio output");
    let sound = SineWave::new(FREQUENCY_HZ).amplify(AMPLIFICATION);
    stream_handle.play_raw(sound).expect("playing sound");
    sleep(SLEEP_DURATION);
}
