#!/bin/bash

set -euxo pipefail

ssh euridice systemctl stop poplessdsp.service
scp target/aarch64-unknown-linux-gnu/release/poplessdsp euridice:/opt/
scp poplessdsp.service euridice:/etc/systemd/system/
ssh euridice systemctl daemon-reload
ssh euridice systemctl enable --now poplessdsp.service
